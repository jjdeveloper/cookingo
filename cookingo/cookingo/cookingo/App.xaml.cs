﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cookingo.Pages;
using Xamarin.Forms;

namespace cookingo {
    public partial class App : Application {

        public App() {
            // var _mainPage = new cookingo.AllRecipesPage();
            InitializeComponent();
            MainPage = new MasterPage();
        }

        protected override void OnStart() {
            //main.Load();
        }

        protected override void OnSleep() {
            // Handle when your app sleeps
        }

        protected override void OnResume() {
            // Handle when your app resumes
        }
    }
}