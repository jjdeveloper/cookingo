﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cookingo.Classes;
using Xamarin.Forms;

namespace cookingo.Pages {

    public partial class MasterPage : MasterDetailPage {

        public MasterPage() {

            InitializeComponent();

            Master = new MenuPage();
            Detail = new NavigationPage( new AllRecipesPage() );
        }
    }
}