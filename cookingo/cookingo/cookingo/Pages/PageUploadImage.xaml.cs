﻿using cookingo.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace cookingo.Pages
{
    public partial class PageUploadImage : ContentPage
    {

        static Stream streamCopy;
        public PageUploadImage()
        {
            InitializeComponent();
        }

        async void btnFoto_Clicked(object sender, EventArgs e)
        {
            var useCamera = ((Button)sender).Text.Contains("foto");
            var file = await ImageServices.TomarFoto(useCamera);
            Recetas.Children.Clear();



            imgFoto.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                streamCopy = new MemoryStream();
                stream.CopyTo(streamCopy);
                stream.Seek(0, SeekOrigin.Begin);
                file.Dispose();
                return stream;

            });
        }

        async void btnSubirFoto_Clicked(object sender, EventArgs e)
        {
            if (streamCopy != null)
            {
                streamCopy.Seek(0, SeekOrigin.Begin);
                //aca habria que pasarselo al blob (generando un metodo)
            }
        }
    }
}
