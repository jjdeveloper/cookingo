﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cookingo;
using cookingo.Classes;
using Xamarin.Forms;
using System.Windows.Input;

namespace cookingo.Pages {
    public partial class AllRecipesPage : ContentPage {
        protected AzureClient DataService { get; set; }
        public ObservableCollection<Recipe> Items { get; set; }
        public Command RefreshCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ObservableCollection<RecipeViewModel> Recetas { get; private set; }
        public NavigationService NavigationService { get; set; }
        private AzureClient _client;

        public AllRecipesPage() {

            this.Items = new ObservableCollection<Recipe>();
            this.RefreshCommand = new Command( () => Load() );
            this._client = new AzureClient();
            SearchCommand = new Command(obj => CallSearch());
            this.BindingContext = this;
            Recetas = new ObservableCollection<RecipeViewModel>();
            NavigationService = new NavigationService(Navigation);
            InitializeComponent();

            Load();
        }

        private async void CallRecipes()
        {
            if (Recetas.Any())
            {
                //   Indicator.IsVisible = false;
            }

            IsBusy = true;
            //  List.IsVisible = true;
            //   Response.Text = string.Empty;

            try
            {
                var result = await DataService.GetRecipes();

                Recetas.Clear();

                foreach (var item in result)
                {
                    Recetas.Add(new RecipeViewModel(item)
                    {
                        // Image = ImageSource.FromUri(DataService.GetRecipes(item.Image)),
                    });
                }
                // Response.Text = string.Empty;
                // StatusPanel.IsVisible = false;
            }
            catch (Exception ex)
            {
                //  Response.Text = ex.Message;
                // StatusPanel.IsVisible = true;
                // List.IsVisible = false;
            }
            finally
            {
                IsBusy = false;
                // CallButton.Text = "Refresh";
            }
        }

        private async void CallSearch()
        {
            var searchPage = new SearchPage();
            searchPage.Recetas = Recetas;

            await NavigationService.PushAsync(searchPage);
        }

        public async void Load() {
            this.IsBusy = true;

            this.Items.Clear();

            var result = await _client.GetRecipes();

            foreach( var item in result )
                this.Items.Add( item );

            this.IsBusy = false;
        }
    }
}