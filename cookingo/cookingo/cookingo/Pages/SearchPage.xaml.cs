﻿namespace cookingo
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;

    public partial class SearchPage : INotifyPropertyChanged
    {
        public NavigationService NavigationService { get; set; }
        
        public SearchPage()
        {
            NavigationService = new NavigationService(Navigation);
            Results = new ObservableCollection<RecipeViewModel>();

            InitializeComponent();
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                OnPropertyChanged(nameof(SearchText));
                Filter();
            }
        }

        public ObservableCollection<RecipeViewModel> Results { get; private set; }

        private ObservableCollection<RecipeViewModel> _recetas;
        public ObservableCollection<RecipeViewModel> Recetas
        {
            get { return _recetas; }
            set
            {
                _recetas = value;
                OnPropertyChanged(nameof(Recetas));

                SearchText = string.Empty;
            }
        }

        private void Filter()
        {
            Results.Clear();
            
            var query = string.IsNullOrEmpty(SearchText) ?
                              Recetas.ToArray()
                              : Recetas.Where(item => item.Matches(SearchText)).ToArray();

            foreach (var item in query)
            {
                Results.Add(item);
            }
        }
    }
}