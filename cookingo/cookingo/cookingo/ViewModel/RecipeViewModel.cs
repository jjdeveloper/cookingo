﻿using cookingo.Classes;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace cookingo
{
    public class RecipeViewModel
    {
        public Recipe Model { get; set; }
        public ImageSource Image { get; set; }
        public ICommand BrowseCommand { get; set; }
        public ObservableCollection<RecipeDetail> Details { get; set; }
        public ObservableCollection<ImageDetail> Photos { get; set; }
        public string Title { get; private set; }

        public RecipeViewModel(Recipe model)
        {
            Title = "Receta: " + model.Title;

            Model = model;

            Details = new ObservableCollection<RecipeDetail>(new[] {
                new RecipeDetail("Título", Model.Title),
                new RecipeDetail("Descripción", Model.Description),
                new RecipeDetail("Tiempo de cocción", Model.CookingTime),
            });

            Photos = new ObservableCollection<ImageDetail>(new ImageDetail[] {
                new ImageDetail { Image = Image},
                new ImageDetail { Image = Image},
                new ImageDetail { Image = Image}
            });
        }

        internal bool Matches(string searchText)
        {
            return Model != null &&
                (Model.Title ?? string.Empty).ToLowerInvariant().Contains(searchText.ToLowerInvariant());
        }
    }
}