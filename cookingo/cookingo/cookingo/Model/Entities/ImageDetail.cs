﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cookingo.Classes
{
    public class ImageDetail
    {
        public string Title { get; set; }
        public ImageSource Image { get; set; }
    }
}
