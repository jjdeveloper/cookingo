﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cookingo
{
    [DataTable("Recetas")]
    public class Recipe
    {
        [JsonProperty]
        public string Id { get; set; }

        [JsonProperty]
        public string Title { get; set; }

         [JsonProperty]
        public string Description { get; set; }

        [JsonProperty]
         public string Image { get; set; }

        [JsonProperty]
        public string CookingTime { get; set; }

        [Microsoft.WindowsAzure.MobileServices.Version]
        public string AzureVersion { get; set; }
    }
}
