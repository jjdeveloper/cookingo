﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cookingo
{
    public class RecipeDetail
    {
        public RecipeDetail(string title, object value)
        {
            Title = title;
            Value = value;
        }

        public string Title { get; set; }
        public object Value { get; set; }
        public ImageSource Image { get; set; }
    }
}
