﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using cookingo;

namespace cookingo.Classes
{
    public class AzureClient
    {
        private IMobileServiceClient _client;
        private IMobileServiceTable<Recipe> _table;

        public AzureClient()
        {
            _client = new MobileServiceClient("http://cookingo.azurewebsites.net");
            _table = _client.GetTable<Recipe>();
        }

        public Task<IEnumerable<Recipe>> GetRecipes()
        {
            return _table.ToEnumerableAsync();
        }


    }
}
